package com.tposiadala.githubapitask.exception;

import com.tposiadala.githubapitask.api.dto.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Arrays;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleOtherExceptions(Exception ex, WebRequest webRequest) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .error(HttpStatus.NOT_FOUND.getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .path(((ServletWebRequest)webRequest).getRequest().getRequestURI())
                .message(ex.getMessage())
                .debugMessage(ex.getLocalizedMessage())
                .build();
        log.error("Stack trace of the error: " + Arrays.toString(ex.getStackTrace()));
        return handleExceptionInternal(ex, apiError,
                new HttpHeaders(), HttpStatus.NOT_FOUND, webRequest);
    }
}
