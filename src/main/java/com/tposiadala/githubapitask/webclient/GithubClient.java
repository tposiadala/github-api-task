package com.tposiadala.githubapitask.webclient;

import com.tposiadala.githubapitask.webclient.dto.UserDetailsDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class GithubClient {

    private static final String GITHUB_URL = "https://api.github.com/users/%s";
    private final RestTemplate restTemplate = new RestTemplate();

    public Optional<UserDetailsDto> getUserDetails(String login) {
        log.info("Making API calls to get Github user details: [GET] " + getUrl(login));
        ResponseEntity<UserDetailsDto> result = restTemplate.exchange(
                getUrl(login),
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                UserDetailsDto.class
        );
        return Optional.ofNullable(result.getBody());
    }

    private String getUrl(String login) {
        return String.format(GITHUB_URL, login);
    }
}
