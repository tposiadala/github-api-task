package com.tposiadala.githubapitask.api.mapper;

import com.tposiadala.githubapitask.api.dto.UserDto;
import com.tposiadala.githubapitask.webclient.dto.UserDetailsDto;

public class UserMapper {

    public static UserDto mapToUserDto(UserDetailsDto userInfo) {
        return UserDto.builder()
                .id(userInfo.getId())
                .login(userInfo.getLogin())
                .name(userInfo.getName())
                .type(userInfo.getType())
                .avatarUrl(userInfo.getAvatarUrl())
                .createdAt(userInfo.getCreatedAt())
                .build();
    }
}
