package com.tposiadala.githubapitask.repository;

import com.tposiadala.githubapitask.domain.UserRequestCounter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRequestCounterRepository extends JpaRepository<UserRequestCounter, String> {

    @Query("Select u From user_request_counter u WHERE login = :login")
    Optional<UserRequestCounter> findByLogin(String login);
}
