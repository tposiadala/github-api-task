package com.tposiadala.githubapitask.service;

import com.tposiadala.githubapitask.api.dto.UserDto;
import com.tposiadala.githubapitask.api.mapper.UserMapper;
import com.tposiadala.githubapitask.webclient.GithubClient;
import com.tposiadala.githubapitask.webclient.dto.UserDetailsDto;
import com.tposiadala.githubapitask.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private static final int FIRST_MAGIC_NUMBER = 6;
    private static final int SECOND_MAGIC_NUMBER = 2;

    private final UserRequestCounterService userRequestCounterService;
    private final GithubClient githubClient;

    public UserDto getUserAndUpdateRequestCounter(String login) {
        userRequestCounterService.updateOrCreateRequestCounter(login);
        UserDetailsDto userInfo = githubClient.getUserDetails(login).orElseThrow(() -> new UserNotFoundException(login));

        UserDto userDto = UserMapper.mapToUserDto(userInfo);
        userDto.setCalculations(getCalculations(userInfo.getFollowers(), userInfo.getPublicRepos()));
        return userDto;
    }

    private BigDecimal getCalculations(double followers, double publicRepos) {
        if (followers == 0) {
            return BigDecimal.ZERO;
        }
        return BigDecimal.valueOf(FIRST_MAGIC_NUMBER / followers * (SECOND_MAGIC_NUMBER + publicRepos));
    }
}
