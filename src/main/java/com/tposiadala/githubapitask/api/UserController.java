package com.tposiadala.githubapitask.api;

import com.tposiadala.githubapitask.api.dto.UserDto;
import com.tposiadala.githubapitask.service.UserFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/users")
@RestController
public class UserController {

    private final UserFacade userFacade;

    @GetMapping("/{login}")
    public ResponseEntity<UserDto> getUser(@PathVariable String login) {
        return ResponseEntity.ok(userFacade.getUserAndUpdateRequestCounter(login));
    }
}
