package com.tposiadala.githubapitask.service;

import com.tposiadala.githubapitask.domain.UserRequestCounter;
import com.tposiadala.githubapitask.repository.UserRequestCounterRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserRequestCounterService {

    private final UserRequestCounterRepository repository;

    public synchronized void updateOrCreateRequestCounter(String login) {
        repository.findByLogin(login)
                .map(element -> {
                    log.info("Updating counter for user: " + login);
                    element.setRequestCount(element.getRequestCount() + 1);
                    return element;
                })
                .orElseGet(() -> {
                    log.info("Creating new row for user: " + login);
                    UserRequestCounter newUserRequestCounter = new UserRequestCounter();
                    newUserRequestCounter.setLogin(login);
                    newUserRequestCounter.setRequestCount(1);
                    return repository.save(newUserRequestCounter);
                });
    }
}
